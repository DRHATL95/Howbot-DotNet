﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Howbot.Interfaces;
using Howbot.Modules;
using Howbot.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System.Reflection;
using Victoria;

namespace Howbot
{
    internal sealed class Program
    {

        // Main entry point
        static void Main(string[] args)        
            => new Program().MainAsync().GetAwaiter().GetResult();        

        private async Task MainAsync()
        {
            try {
                var configurationPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                // Create reference to appsettings.json for Serilog Logger
                var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile(path: "appsettings.json", optional: false, reloadOnChange: true)
                    .Build();

                // Create Serilog instance
                Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(configuration)
                    .CreateLogger();

                // Get services from DependencyInjection
                using (var services = ConfigureServices()) {
                    var client = services.GetRequiredService<DiscordSocketClient>();

                    client.Log += LogAsync;

                    await client.LoginAsync(TokenType.Bot, Environment.GetEnvironmentVariable("DiscordTokenDev"));
                    await client.StartAsync();

                    var commandService = services.GetRequiredService<CommandService>();

                    if (!commandService.Modules.Any()) {
                        // Add Modules to Discord.Commands explicitly
                        await commandService.AddModuleAsync(typeof(MusicModule), services);
                    }

                    await services.GetRequiredService<CommandHandlerService>().OnInitialized();
                    // services.GetRequiredService<IAudioService>();

                    var lavaPlayer = services.GetRequiredService<LavaNode>();

                    client.Ready += async () => {
                        if (!lavaPlayer.IsConnected) {
                            await lavaPlayer.ConnectAsync();
                        }
                    };

                    await Task.Delay(Timeout.Infinite);
                }
            } catch (Exception) {
                throw;
            }
        }

        private static ServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddSingleton<DiscordSocketClient>()
                .AddSingleton(new CommandService(new CommandServiceConfig()
                {
                    CaseSensitiveCommands = false,
                    LogLevel = LogSeverity.Debug
                }))
                .AddSingleton<AudioService>()
                .AddSingleton<CommandHandlerService>()
                .AddSingleton<EmbedService>()
                .AddSingleton<HttpClient>()
                .AddLavaNode(x => { x.SelfDeaf = true; })
                .BuildServiceProvider();
        }

        #region Events

        private Task LogAsync(LogMessage log)
        {
            Console.WriteLine(log.ToString());

            return Task.CompletedTask;
        }

        #endregion
    }
}