﻿using Victoria;
using Victoria.EventArgs;

namespace Howbot.Interfaces
{
    public interface IAudioService
    {
        Task OnPlayerUpdated(PlayerUpdateEventArgs args);
        Task OnStatsReceived(StatsEventArgs args);
        Task OnTrackStarted(TrackStartEventArgs args);
        Task OnTrackEnded(TrackEndedEventArgs args);
        Task InitiateDisconnectAsync(LavaPlayer player, TimeSpan timeSpan);
        Task OnTrackException(TrackExceptionEventArgs args);
        Task OnTrackStuck(TrackStuckEventArgs args);
        Task OnWebSocketClosed(WebSocketClosedEventArgs args);
    }
}
