﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace Howbot.Interfaces
{
    public interface ICommandHandlerService
    {
        Task OnInitialized();
        Task MessageReceivedAsync(SocketMessage rawMessage);
        Task CommandExecutedAsync(Optional<CommandInfo> command, ICommandContext commandContext, IResult commandResult);
        Task LogAsync(LogMessage logMessage);
    }
}
