﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Howbot.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Howbot.Services
{
    public sealed class CommandHandlerService : ICommandHandlerService, IDisposable
    {
        private readonly CommandService? _commandService;
        private readonly DiscordSocketClient? _client;
        private readonly IServiceProvider? _serviceProvider;

        public CommandHandlerService(IServiceProvider serviceProvider)
        {
            try
            {
                _serviceProvider = serviceProvider;
                _commandService = _serviceProvider?.GetRequiredService<CommandService>();
                _client = _serviceProvider?.GetRequiredService<DiscordSocketClient>();

                if (_commandService != null)
                {
                    // Hook Discord.Net command events
                    _commandService.CommandExecuted += CommandExecutedAsync;
                    _commandService.Log += LogAsync;
                }

                if (_client != null)
                {
                    // Hook Discord.Net client events
                    _client.MessageReceived += MessageReceivedAsync;
                }
            }
            catch (Exception)
            {
                // TODO
            }
        }

        public async Task OnInitialized()
        {
            try
            {
                if (_commandService != null)
                {
                    // Register modules that are public and inherit ModuleBase<T>.
                    _ = await _commandService.AddModulesAsync(Assembly.GetEntryAssembly(), _serviceProvider);
                }                
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Events

        public async Task MessageReceivedAsync(SocketMessage rawMessage)
        {
            // Ignore system messages, or messages from other bots
            if (!(rawMessage is SocketUserMessage message))
                return;
            if (message.Source != MessageSource.User)
                return;
            if (_client == null || _commandService == null)
                return;

            // This value holds the offset where the prefix ends
            var argPos = 0;
            // Perform prefix check. You may want to replace this with
            // (!message.HasCharPrefix('!', ref argPos))
            // for a more traditional command format like !help.
            if (!message.HasMentionPrefix(_client.CurrentUser, ref argPos) && !message.HasCharPrefix('%', ref argPos))
                return;

            var context = new SocketCommandContext(_client, message);

            // Perform the execution of the command. In this method,
            // the command service will perform precondition and parsing check
            // then execute the command if one is matched.
            await _commandService.ExecuteAsync(context, argPos, _serviceProvider);
            // Note that normally a result will be returned by this format, but here
            // we will handle the result in CommandExecutedAsync            
        }

        public async Task CommandExecutedAsync(Optional<CommandInfo> command, ICommandContext context, Discord.Commands.IResult result)
        {
            // command is unspecified when there was a search failure (command not found); we don't care about these errors
            if (!command.IsSpecified)
                return;

            // the command was successful, we don't care about this result, unless we want to log that a command succeeded.
            if (result.IsSuccess)
                return;

            // the command failed, let's notify the user that something happened.
            await context.Channel.SendMessageAsync($"error: {result}");
        }

        public Task LogAsync(LogMessage message)
        {
            if (message.Exception is CommandException cmdException)
            {
                Console.WriteLine($"[Command/{message.Severity}] {cmdException.Command.Aliases.First()}"
                    + $" failed to execute in {cmdException.Context.Channel}.");
                Console.WriteLine(cmdException);
            }
            else
                Console.WriteLine($"[General/{message.Severity}] {message}");

            return Task.CompletedTask;
        }

        #endregion

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
