﻿using Howbot.Interfaces;
using System.Collections.Concurrent;
using Victoria;
using Victoria.EventArgs;

namespace Howbot.Services
{
    public sealed class AudioService : IAudioService, IDisposable
    {
        private readonly ConcurrentDictionary<ulong, CancellationTokenSource> _disconnectTokens;
        private readonly LavaNode _lavaNode;

        public AudioService(LavaNode lavaNode)
        {
            _lavaNode = lavaNode;
            _disconnectTokens = new ConcurrentDictionary<ulong, CancellationTokenSource>();

            _lavaNode.OnPlayerUpdated += OnPlayerUpdated;
            _lavaNode.OnStatsReceived += OnStatsReceived;
            _lavaNode.OnTrackEnded += OnTrackEnded;
            _lavaNode.OnTrackStarted += OnTrackStarted;
            _lavaNode.OnTrackException += OnTrackException;
            _lavaNode.OnTrackStuck += OnTrackStuck;
            _lavaNode.OnWebSocketClosed += OnWebSocketClosed;
        }

        public void Dispose()
        {
            try
            {
                if (_lavaNode != null)
                {
                    _lavaNode.OnPlayerUpdated -= OnPlayerUpdated;
                    _lavaNode.OnStatsReceived -= OnStatsReceived;
                    _lavaNode.OnTrackEnded -= OnTrackEnded;
                    _lavaNode.OnTrackStarted -= OnTrackStarted;
                    _lavaNode.OnTrackException -= OnTrackException;
                    _lavaNode.OnTrackStuck -= OnTrackStuck;
                    _lavaNode.OnWebSocketClosed -= OnWebSocketClosed;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task InitiateDisconnectAsync(LavaPlayer player, TimeSpan timeSpan)
        {
            try
            {
                if (!_disconnectTokens.TryGetValue(player.VoiceChannel.Id, out var value))
                {
                    value = new CancellationTokenSource();
                    _disconnectTokens.TryAdd(player.VoiceChannel.Id, value);
                }
                else if (value.IsCancellationRequested)
                {
                    _disconnectTokens.TryUpdate(player.VoiceChannel.Id, new CancellationTokenSource(), value);
                    value = _disconnectTokens[player.VoiceChannel.Id];
                }

                await player.TextChannel.SendMessageAsync($"Auto disconnect initiated! Disconnecting in {timeSpan}...");
                var isCancelled = SpinWait.SpinUntil(() => value.IsCancellationRequested, timeSpan);
                if (isCancelled)
                    return;

                await _lavaNode.LeaveAsync(player.VoiceChannel);
                await player.TextChannel.SendMessageAsync("Leaving voice channel!");
            }
            catch (Exception)
            {

                throw;
            }
        }

        #region Events

        public Task OnPlayerUpdated(PlayerUpdateEventArgs arg)
        {
            try
            {
                if (arg.Track != null && arg.Position != null)
                {
                    Console.WriteLine($"Track update received for {arg.Track.Title}: {arg.Position}");
                }

                return Task.CompletedTask;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task OnStatsReceived(StatsEventArgs args)
        {
            try
            {
                Console.WriteLine($"Lavalink has been up for {args.Uptime}.");
                return Task.CompletedTask;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task OnTrackEnded(TrackEndedEventArgs args)
        {
            try
            {
                if (args.Reason != Victoria.Enums.TrackEndReason.Finished)
                {
                    return;
                }

                var player = args.Player;
                if (!player.Queue.TryDequeue(out var lavaTrack))
                {
                    await player.TextChannel.SendMessageAsync("Queue empty. Please add more tracks!");
                    await InitiateDisconnectAsync(args.Player, TimeSpan.FromSeconds(10));
                    return;
                }

                if (lavaTrack is null)
                {
                    await player.TextChannel.SendMessageAsync("Next item in queue is not a track.");
                    return;
                }

                await args.Player.PlayAsync(lavaTrack);
                await args.Player.TextChannel.SendMessageAsync(
                    $"{args.Reason}: {args.Track.Title}\nNow playing: {lavaTrack.Title}");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task OnTrackException(TrackExceptionEventArgs args)
        {
            try
            {
                Console.WriteLine($"Track {args.Track.Title} threw an exception. Please check Lavalink console/logs.");

                args.Player.Queue.Enqueue(args.Track);
                await args.Player.TextChannel.SendMessageAsync(
                    $"{args.Track.Title} has been re-added to queue after throwing an exception.");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task OnTrackStarted(TrackStartEventArgs args)
        {
            var textChannel = args.Player.TextChannel;
            var voiceChannel = args.Player.VoiceChannel;

            if (_lavaNode.TryGetPlayer(args.Player.VoiceChannel.Guild, out var lavaPlayer))
            {
                await textChannel.SendMessageAsync($"Now playing: {lavaPlayer.Track.Title}");
            }

            if (!_disconnectTokens.TryGetValue(voiceChannel.Id, out var value))
                return;
            if (value.IsCancellationRequested)
                return;

            value.Cancel(true);
            await textChannel.SendMessageAsync("Auto disconnect has been cancelled!");
        }

        public async Task OnTrackStuck(TrackStuckEventArgs args)
        {
            try
            {
                Console.WriteLine(
                $"Track {args.Track.Title} got stuck for {args.Threshold}ms. Please check Lavalink console/logs.");

                args.Player.Queue.Enqueue(args.Track);
                await args.Player.TextChannel.SendMessageAsync(
                    $"{args.Track.Title} has been re-added to queue after getting stuck.");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Task OnWebSocketClosed(WebSocketClosedEventArgs args)
        {
            try
            {
                Console.WriteLine($"Discord WebSocket connection closed with following reason: {args.Reason}");
                return Task.CompletedTask;
            }
            catch (Exception)
            {

                throw;
            }
        }


        #endregion
    }
}
